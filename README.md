# Android-storage-example

More info at http://kilatas.great-site.net/diy.html?i=1 (Basic storage handling on Android)

I created this sample code because the usage of Storage Access Framework is mandatory on Google Play since Q2 of 2021. As i modified my app (https://play.google.com/store/apps/details?id=com.barcodenote) according to use Scoped storage (which is part of SAF), i searched a lot for appropriate file handling methods. Due to finding many outdated examples, i wrote a simple example code for using basic file input/output fuctions in Android Scoped storage. 

In Scoped Storage, the point is that an app will basically access files in its own directory under /storage/emulated/0/Android/data/com.appname/files , but for simple cases, this can be enough. In this code the readfile, writefile, methods are for file operations, and isReadStoragePermissionGranted(), isWriteStoragePermissionGranted() methods are usable to check if the permissions are granted. onrequestpermissionresult() is also useful on the app's first run. These can be found in Android_storage_example/app/src/main/java/com//example/storageexample/MainActivity.java.

The code is uploaded az an Android Studio project, so after download, you can import it to Studio.

The sample app is displaying the app's directory path, creating a test file, writing the actual time into it, reading it back, and displaying. 

Code is free to use with noting the developer's name (Creative Commons license).

Resources i have used : 

http://developer.android.com/guide/topics/data/data-storage.html

http://stackoverflow.com/questions/3551821/android-write-to-sd-card-folder

https://stackoverflow.com/questions/12421814/how-can-i-read-a-text-file-in-android

App should look like this : 


![SAMPLE_IMAGE](https://gitlab.com/colt2x/Android_storage_example/-/raw/main/Screenshot_20210516-140158_My_Application.png)



