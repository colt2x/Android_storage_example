//Resources
// See http://developer.android.com/guide/topics/data/data-storage.html#filesExternal
// See http://stackoverflow.com/questions/3551821/android-write-to-sd-card-folder

package com.example.storageexample;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;


public class MainActivity extends AppCompatActivity {

    File file;
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = (TextView) findViewById(R.id.TV);

        //Check permissions, request if none
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
            }
        else {
            //Call the main routine if already have
            access_storage_file();
        }


    }

//Main routine
    public void access_storage_file() {
        //Variable declaration
        String file_name="testfile.txt";
        String appdirpath= String.valueOf(getExternalFilesDir(null)); //Directory of application's files
        String datestring=String.valueOf(Calendar.getInstance().getTime()); //Current time

        //Display app directory path + write current time to file
        tv.setText("Application files directory is : " + appdirpath + "\n\n\n");
        tv.append("Current time is " +  datestring);
        tv.append("\nTrying to write current time to : " + file_name + "\n\n");
        //You can call the write method with a file name and a string variable containing your data
        writefile(file_name,datestring);

        //Read back what was written
        tv.append("\nTrying to read file : " + file_name + "\n");
        //The reader returns a string, read from the given filename
        String read=readfile(file_name);

        //Display the content of the test file
        tv.append(read);

    }



    //File writer method, doesn't care about if file exists, or media is writeable. Parameters are two strings, filename and data to write
    public void writefile(String filename,String data) {
        //Assign the file
        file = new File(getExternalFilesDir(null), filename);
        try {
            //Open output stream and create the writer
            FileOutputStream f = new FileOutputStream(file);
            PrintWriter pw = new PrintWriter(f);
            //Write the data
            pw.println(data);
            //Flush data, close the writer and the file
            pw.flush();
            pw.close();
            f.close();
        } catch (FileNotFoundException e) {
            Toast.makeText(getApplicationContext(),filename + " cannot be written", Toast.LENGTH_LONG).show();
            //Error handling here
            e.printStackTrace();
        } catch (IOException e) {
            //Error handling here
            Toast.makeText(getApplicationContext(),"I/O error on " + filename, Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }



    //Reader method
    public String readfile(String filename) {
        String data = "";
        //Assign file
        file = new File(getExternalFilesDir(null), filename);
        //Does not read if file exists
        if (file.exists() ) {
            try {
                //Initialize the reader, and read the file line by line to the data variable until end of file
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;
                while ((line = br.readLine()) != null) {
                    data = data + line;
                }
                //Close the reader if EOF
                br.close();
            } catch (
                    IOException e) {
                Toast.makeText(getApplicationContext(),filename + " cannot be read :(", Toast.LENGTH_LONG).show();
                //Error handling here
            }
        }
        //If file cannot be found, make a toast
        else { Toast.makeText(getApplicationContext(),"File" + String.valueOf(getExternalFilesDir(null) + filename) + " does not exist", Toast.LENGTH_LONG).show(); }
        return (data); //Return the contents
    }


    //This is caled when the permission request is finished, e.g. the user responded
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // If the permissions are not granted, exit
        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
            Toast.makeText(getApplicationContext(),"Storage access permission not granted :(", Toast.LENGTH_LONG).show();
            finish();
        }

        //If the permissions are OK,
        if (isReadStoragePermissionGranted() && (isWriteStoragePermissionGranted())) {
            //call the main routine
            access_storage_file();

        }

    }

    //Check if storage read permission is granted. If not, request is
    public boolean isReadStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
                return false;
            }
        } else {
            //Permission is automatic over SDK 23
            return true;
        }
    }

    //Check if storage read permission is granted. If not, request is
    public boolean isWriteStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);
                return false;
            }
        } else { //Permission is automatic over SDK 23
            return true;
        }
    }

}